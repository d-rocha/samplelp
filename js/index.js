"use strict";

function formValidate() {
  let form = document.contact;
  let fName = form.firstName;
  let lName = form.lastName;
  let email = form.email;
  let tel = form.telephone;

  if (fName.value == "") {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "First name field must not be empty"
    });
    return false;
  }

  if (lName.value == "") {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Last name field must not be empty"
    });
    return false;
  }

  if (
    email.value == "" ||
    email.value.indexOf("@") == -1 ||
    email.value.indexOf(".") == -1
  ) {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Enter a valid email"
    });
    return false;
  }

  if (tel.value == "") {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Telephone field must not be empty"
    });
    return false;
  }

  return true;
}

function validatePhone(value) {
  const mask = {
    telephone(data) {
      return data
        .replace(/\D/g, "")
        .replace(/(\d{3})(\d)/, "($1) $2")
        .replace(/(\d{3})(\d)/, "$1-$2")
        .replace(/(-\d{4})(\d+?)$/, "$1");
    }
  };

  document.querySelectorAll("input").forEach($input => {
    const field = $input.dataset.js;

    $input.addEventListener(
      "input",
      e => {
        e.target.value = mask[field](e.target.value);
      },
      false
    );
  });
  return value;
}